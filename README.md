Task:
write simple iOS project that fetches JSON data from network and maps it to Objective-C classes
1. no UI required
2. app should read information from http://jsonplaceholder.typicode.com/users, http://jsonplaceholder.typicode.com/posts 
3. Result should be converted to hierarchy of User array, that contain Post array for every user

4. User and Post should map JSON structure  
5. User and Post should have initWithDictionary: method and keep actual data as Dictionary
6. User and Post should support changes of property values using @dynamic mechanism

7. Nice to have are Unit-tests that verify “happy” and “unhappy” paths