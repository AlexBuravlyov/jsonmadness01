//
//  Business.m
//  JsonMadness01
//
//  Created by zgushonka on 3/4/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import "Business.h"
#import "Network.h"
#import "User.h"
#import "Post.h"

#define USERS_URL_STRING @"http://jsonplaceholder.typicode.com/users"
#define POSTS_URL_STRING @"http://jsonplaceholder.typicode.com/posts"

@interface Business ()
@property (strong, nonatomic) Network *network;

@property (strong, atomic) NSArray<User *> *users;
@property (strong, atomic) NSArray<Post *> *posts;
@end


@implementation Business

- (instancetype)init {
    self = [super init];
    if (self) {
        self.users = nil;
        self.posts = nil;
    }
    return self;
}

- (Network *)network {
    if (!_network) {
        _network = [[Network alloc] init];
    }
    return _network;
}

- (void)doTheWorkPlease {
    [self p_fetchUsers];
    [self p_fetchPosts];
}

- (void)p_fetchUsers {
    __weak typeof(self) weakSelf = self;
    NSURL *usersUrl = [NSURL URLWithString:USERS_URL_STRING];
    [self.network fetchJsonAtURL:usersUrl callback:^(NSArray *result) {
        [weakSelf p_handleUsersDictionary:result];
    }];
}

- (void)p_handleUsersDictionary:(NSArray *)array {
    NSMutableArray *users = [NSMutableArray array];
    for (id dict in array) {
        if ([dict isKindOfClass:[NSDictionary class]]) {
            User *user = [[User alloc] initWithDictionary:dict];
            [users addObject:user];
        }
    }
    self.users = [users copy];
    
    [self p_fetchDone];
}

- (void)p_fetchPosts {
    __weak typeof(self) weakSelf = self;
    NSURL *postsUrl = [NSURL URLWithString:POSTS_URL_STRING];
    [self.network fetchJsonAtURL:postsUrl callback:^(NSArray *result) {
        [weakSelf p_handlePostsDictionary:result];
    }];
}

- (void)p_handlePostsDictionary:(NSArray *)array {
    NSMutableArray *posts = [NSMutableArray array];
    for (id dict in array) {
        if ([dict isKindOfClass:[NSDictionary class]]) {
            Post *post = [[Post alloc] initWithDictionary:dict];
            [posts addObject:post];
        }
    }
    self.posts = [posts copy];
    
    [self p_fetchDone];
}

- (void)p_fetchDone {
    BOOL allDataFetched = self.users && self.posts;
    if (allDataFetched) {
        [self p_putPostsToUsers];
        [self p_printResultDescription];
    }
}

- (void)p_putPostsToUsers {
    for (User *user in self.users) {
        NSArray<Post *> *posts = [self p_postForUserId:user.id];
        user.posts = posts;
    }
}

- (NSArray<Post *> *)p_postForUserId:(NSNumber *)_id {
    NSMutableArray<Post *> *posts = [NSMutableArray array];
    for (Post *post in self.posts) {
        if (post.userId == _id) {
            [posts addObject:post];
        }
    }
    return [posts copy];
}

- (void)p_printResultDescription {
    for (User *user in self.users) {
        NSLog(@"%@", user.description);
    }
}

@end

