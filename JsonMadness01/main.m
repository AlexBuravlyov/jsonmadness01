//
//  main.m
//  JsonMadness01
//
//  Created by zgushonka on 3/4/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
