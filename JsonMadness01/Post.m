//
//  Post.m
//  JsonMadness01
//
//  Created by zgushonka on 3/5/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import "Post.h"

@implementation Post

@dynamic userId;
@dynamic title;
@dynamic body;

- (NSString *)description {
    NSString *description = [NSString stringWithFormat:@"Postid: %@\nTitle:\n%@\nBody:\n%@", self.id, self.title, self.body];
    return description;
}

@end
