//
//  Network.h
//  JsonMadness01
//
//  Created by zgushonka on 3/4/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Network : NSObject

- (void)fetchJsonAtURL:(NSURL *)url callback:(void(^)(NSArray *result))callback;

@end
