//
//  User.m
//  JsonMadness01
//
//  Created by zgushonka on 3/5/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic name;
@dynamic username;
@dynamic email;
@dynamic phone;
@dynamic website;
@dynamic address;
@dynamic company;
@dynamic posts;

- (NSString *)description {
    NSString *description = [NSString stringWithFormat:@"UserId: %@, UserName: %@", self.id, self.name];
    for (Post *post in self.posts) {
        description = [NSString stringWithFormat:@"%@\n%@\n", description, post.description];
    }
    return description;
}

@end

