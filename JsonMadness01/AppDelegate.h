//
//  AppDelegate.h
//  JsonMadness01
//
//  Created by zgushonka on 3/4/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

