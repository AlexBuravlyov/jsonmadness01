//
//  BaseEntity.m
//  JsonMadness01
//
//  Created by zgushonka on 3/6/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import "BaseEntity.h"
#import <objc/runtime.h>


@interface BaseEntity ()
@property (strong) NSMutableDictionary *properties;
@end


@implementation BaseEntity

@dynamic id;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.properties = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    }
    return self;
}

static id propertyIMP(id self, SEL _cmd) {
    return [[self properties] valueForKey: NSStringFromSelector(_cmd)];
}

static void setPropertyIMP(id self, SEL _cmd, id aValue) {
    id value = [aValue copy];
    
    NSMutableString *key = [NSStringFromSelector(_cmd) mutableCopy];
    
    // Delete "set" and ":" and lowercase first letter
    [key deleteCharactersInRange:NSMakeRange(0, 3)];
    [key deleteCharactersInRange:NSMakeRange([key length] - 1, 1)];
    NSString *firstChar = [key substringToIndex:1];
    [key replaceCharactersInRange:NSMakeRange(0, 1) withString:[firstChar lowercaseString]];
    
    [[self properties] setValue:value forKey:key];
}

+ (BOOL)resolveInstanceMethod:(SEL)aSEL {
    BOOL isSetter = [NSStringFromSelector(aSEL) hasPrefix:@"set"];
    if (isSetter) {
        class_addMethod([self class], aSEL, (IMP)setPropertyIMP, "v@:@");
    }
    else {
        class_addMethod([self class], aSEL, (IMP)propertyIMP, "@@:");
    }
    return YES;
}

@end

