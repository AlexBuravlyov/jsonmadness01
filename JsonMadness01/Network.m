//
//  Network.m
//  JsonMadness01
//
//  Created by zgushonka on 3/4/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import "Network.h"

#define LOG_ERROR(e) NSLog(@"%s line %d \nERROR: %@", __FUNCTION__, __LINE__, [e localizedDescription])


@implementation Network

- (void)fetchJsonAtURL:(NSURL *)url callback:(void (^)(NSArray *))callback {
    
    if (!url) {
        NSLog(@"%s\nWarning: no URL", __FUNCTION__);
        return;
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
        if (error) {
            LOG_ERROR(error);
            return;
        }
        
        NSError *jsonError;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) {
            LOG_ERROR(jsonError);
            return;
        }
        
        if ([jsonObject isKindOfClass:[NSArray class]]) {
            NSArray *json = (NSArray *)jsonObject;
            if (callback != NULL) {
                callback(json);
            }
        }
        else {
            NSLog(@"%s\nWarning: we really expect an array here.", __FUNCTION__);
        }
        
    }];
    
    [dataTask resume];
}

@end


