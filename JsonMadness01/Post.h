//
//  Post.h
//  JsonMadness01
//
//  Created by zgushonka on 3/5/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface Post : BaseEntity

@property (copy) NSNumber *userId;
@property (copy) NSString *title;
@property (copy) NSString *body;

@end

/*
 {
 body = "quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error";
 id = 10;
 title = "optio molestias id quia eum";
 userId = 1;
 }
 */

