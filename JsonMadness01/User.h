//
//  User.h
//  JsonMadness01
//
//  Created by zgushonka on 3/5/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
#import "Post.h"

@interface User : BaseEntity

@property (copy) NSString *name;
@property (copy) NSString *username;
@property (copy) NSString *email;
@property (copy) NSString *phone;
@property (copy) NSString *website;
@property (copy) NSDictionary *address;
@property (copy) NSDictionary *company;
@property (copy) NSArray <Post *> *posts;

@end

/*
 {
 "id": 1,
 "name": "Leanne Graham",
 "username": "Bret",
 "email": "Sincere@april.biz",
 "address": {
     "street": "Kulas Light",
     "suite": "Apt. 556",
     "city": "Gwenborough",
     "zipcode": "92998-3874",
     "geo": {
     "lat": "-37.3159",
     "lng": "81.1496"
 }
 },
 "phone": "1-770-736-8031 x56442",
 "website": "hildegard.org",
 "company": {
     "name": "Romaguera-Crona",
     "catchPhrase": "Multi-layered client-server neural-net",
     "bs": "harness real-time e-markets"
 }
 */

