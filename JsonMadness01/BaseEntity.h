//
//  BaseEntity.h
//  JsonMadness01
//
//  Created by zgushonka on 3/6/16.
//  Copyright © 2016 Oleksandr Buravlyov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseEntity : NSObject

@property (copy) NSNumber *id;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
